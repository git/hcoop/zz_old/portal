signature CONTACT =
sig
    type kind = {id :int, name : string, makeUrl : (string * string) option}

    val addKind : string * (string * string) option -> int
    val lookupKind : int -> kind
    val modKind : kind -> unit
    val deleteKind : int -> unit
    val listKinds : unit -> kind list

    datatype priv =
	     PUBLIC
	   | MEMBERS
	   | ADMINS
    val privToInt : priv -> int
    val privFromInt : int -> priv

    type contact = {id :int, usr : int, knd : int, v : string, priv : priv}

    val addContact : int * int * string * priv -> int
    val lookupContact : int -> contact
    val modContact : contact -> unit
    val deleteContact : int -> unit
    val listUserContacts : int * priv -> (kind * contact) list
    val listContactsByKind : int * priv -> (string * contact) list

    val format : kind * contact -> string
end