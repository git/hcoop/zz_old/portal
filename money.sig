signature MONEY =
sig
    type transaction = {id :int, descr : string, amount : real, d : string, stamp : Init.C.timestamp}

    val addTransaction : string * real * string -> int
    val lookupTransaction : int -> transaction
    val modTransaction : transaction -> unit
    val deleteTransaction : int -> unit
    val listTransactions : unit -> transaction list
    val listTransactionsLimit : int -> transaction list
    val listUserTransactions : int -> (real * transaction) list
    val listUserTransactionsLimit : int * int -> (real * transaction) list
    (* Returns list of (your part, overall) pairs *)
    val listUsers : int -> (bool * Init.user) list
    (* List users and indicate whether they participated in a transaction *)

    val lookupHostingUsage : int -> string option

    type charge = {trn : int, usr : int, amount : real}

    val addCharge : charge -> unit
    val listCharges : int -> charge list
    val listChargesWithNames : int -> (string * charge) list

    val clearCharges : int -> unit
    val applyCharges : int -> unit

    val addEvenCharges : int * int list -> unit

    type hosting = {trn : int, cutoff : int, cost : real, usage : string}
    val addHostingCharges : hosting -> unit

    val equalizeBalances : unit -> unit

    val costBase : real -> real
end
