signature LOCATION =
sig
    type location = {id : int, parent : int option, name : string}
    type lives = {usr : int, loc : int}

    val addLocation : int option * string -> int
    val lookupLocation : int -> location
    val modLocation : location -> unit
    val deleteLocation : int -> unit

    val locationTree : int option * int option -> location Util.flat_tree
    val locationTreeWithUser : int option * int option * int -> (bool * location) Util.flat_tree
    val locationTreeWithCounts : int option * int option -> (int * location) Util.flat_tree

    val livesIn : int * int -> bool
    val addToLocation : lives -> unit
    val removeFromLocation : lives -> unit

    val alreadyExists : int option * string -> bool

    val residents : int -> Init.user list
    val residentsOneLevel : int -> Init.user list
    val userLocations : int -> location list
    val subLocations : int option -> location list
end