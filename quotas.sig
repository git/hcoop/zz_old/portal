signature QUOTAS = sig

    val getQuotas : string -> { vol : string,
				used : int,
				quota : int } list

    val path : string -> string

end

