structure Config :> CONFIG = struct

val scratchDir = "/afs/hcoop.net/user/h/hc/hcoop"
val urlPrefix = "https://members.hcoop.net/portal/"
val emailSuffix = "@hcoop.net"
val boardEmail = "board" ^ emailSuffix

val dbstring = "dbname='hcoop_hcoop' user='www-data'"

val kerberosSuffix = "@HCOOP.NET"
		     
val passwordFiles = "/var/lib/portal/"

end
