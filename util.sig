signature UTIL =
sig
    val init : unit -> unit

    datatype 'a flat_element =
	     BEGIN
	   | END
	   | ITEM of 'a
    type 'a flat_tree = 'a flat_element list

    val printInt : int -> unit
    val printReal : real -> unit

    val id : 'a -> 'a
    val makeSet : ('a -> string) -> 'a list -> string
    val neg : real -> real
    val add : real * real -> real
    val mult : int * real -> real

    val validHost : string -> bool
    val validDomain : string -> bool
    val validEmail : string -> bool
    val whoisUrl : string -> string

    val randomPassword : unit -> string

    val domainDir : string -> string

    val readFile : string -> string

    val mem : ''a * ''a list -> bool

    val allLower : string -> string
    val normEmail : string -> string
end
