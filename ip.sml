structure Ip = RequestH(struct
			val table = "Ip"
			val adminGroup = "server"
			fun subject _ = "IP address request"
			val template = "ip"
			val descr = "IP address"

			fun body {node, mail, data = port} =
			    (Mail.mwrite (mail, " IP address for port: ");
			     Mail.mwrite (mail, port);
			     Mail.mwrite (mail, "\n"))
			end)
